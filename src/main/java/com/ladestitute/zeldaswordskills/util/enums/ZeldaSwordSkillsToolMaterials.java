package com.ladestitute.zeldaswordskills.util.enums;

import net.minecraft.block.Blocks;
import net.minecraft.item.IItemTier;
import net.minecraft.item.Item;
import net.minecraft.item.crafting.Ingredient;

public enum ZeldaSwordSkillsToolMaterials implements IItemTier {
    KOKORI_SWORD(4F, 6f, 257, 2, 14, Blocks.AIR.asItem()),
    MASTER_SWORD(8F, 8f, 0, 3, 0, Blocks.AIR.asItem());
    private float attackDmg, efficiency;
    private int durability, harvestLevel, enchantabillity;
    private Item repairMaterial;
    private ZeldaSwordSkillsToolMaterials(float attackDmg, float efficiency, int durability, int harvestLevel, int enchantability, Item repairMaterial) {
        this.attackDmg = attackDmg; // Wood/Gold: 0F, Stone: 1F, Iron: 2F, Diamond: 3F, Netherite: 4F
        this.efficiency = efficiency; // Wood: 2F, Stone: 4F, Iron: 6F, Diamond: 8F, Netherite: 9F, Gold: 12F
        this.durability = durability; // Gold: 32, Wood: 59, Stone: 131, Iron: 250, Diamond: 1561, Netherite: 2031
        this.harvestLevel = harvestLevel; // Wood/Gold: 0, Stone: 1, Iron: 2, Diamond: 3, Netherite: 4
        this.enchantabillity = enchantability; // Stone: 5, Diamond: 10, Iron: 14, Wood/Netherite: 15, Gold: 22,
        this.repairMaterial = repairMaterial;
    }

    @Override
    public int getUses() {
        return this.durability;
    }

    @Override
    public float getSpeed() {
        return this.efficiency;
    }

    @Override
    public float getAttackDamageBonus() {
        return this.attackDmg;
    }

    @Override
    public int getLevel() {
        return this.harvestLevel;
    }

    @Override
    public int getEnchantmentValue() {
        return this.enchantabillity;
    }

    @Override
    public Ingredient getRepairIngredient() {
        return Ingredient.of(this.repairMaterial);
    }

}
