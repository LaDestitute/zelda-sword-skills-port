package com.ladestitute.zeldaswordskills.registry;

import com.ladestitute.zeldaswordskills.ZeldaSwordSkillsMain;
import com.ladestitute.zeldaswordskills.items.swords.KokiriSwordItem;
import com.ladestitute.zeldaswordskills.items.swords.broken.BrokenKokiriSwordItem;
import com.ladestitute.zeldaswordskills.util.enums.ZeldaSwordSkillsToolMaterials;
import net.minecraft.item.Item;
import net.minecraft.item.ItemTier;
import net.minecraft.item.SwordItem;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class ItemInit {
    public static final DeferredRegister<Item> ITEMS = DeferredRegister.create(ForgeRegistries.ITEMS,
            ZeldaSwordSkillsMain.MOD_ID);

    public static final RegistryObject<Item> KOKIRI_SWORD = ITEMS.register("sword_kokiri",
            () -> new KokiriSwordItem(ZeldaSwordSkillsToolMaterials.KOKORI_SWORD, 0, -1f, new SwordItem.Properties().tab(ZeldaSwordSkillsMain.COMBAT_TAB)));

    public static final RegistryObject<Item> BROKEN_KOKIRI_SWORD = ITEMS.register("broken_sword_kokiri",
            () -> new BrokenKokiriSwordItem(new Item.Properties().tab(ZeldaSwordSkillsMain.COMBAT_TAB)));

    //Placeholder master sword for now, its just for the tab icon atm
    public static final RegistryObject<SwordItem> MASTER_SWORD = ITEMS.register("sword_master",
            () -> new SwordItem(ItemTier.NETHERITE, 2, -2.4f, new SwordItem.Properties()));

}
