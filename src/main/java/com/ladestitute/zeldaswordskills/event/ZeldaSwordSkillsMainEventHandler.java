package com.ladestitute.zeldaswordskills.event;

import com.ladestitute.zeldaswordskills.registry.ItemInit;
import net.minecraft.entity.merchant.villager.VillagerEntity;
import net.minecraft.item.ItemStack;
import net.minecraftforge.event.entity.living.LivingAttackEvent;
import net.minecraftforge.event.entity.living.LivingDamageEvent;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class ZeldaSwordSkillsMainEventHandler {

    @SubscribeEvent
    public void cancelleftclicktradedamage(LivingAttackEvent event)
    {
        if(event.getEntityLiving() instanceof VillagerEntity)
        {
            for (ItemStack held : event.getSource().getEntity().getHandSlots())
            {
                if (held.getItem() == ItemInit.BROKEN_KOKIRI_SWORD.get())
                {
                    event.setCanceled(true);
                }
            }
        }
    }
}
