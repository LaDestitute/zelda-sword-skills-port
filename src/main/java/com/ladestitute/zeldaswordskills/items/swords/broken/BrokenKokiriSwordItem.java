package com.ladestitute.zeldaswordskills.items.swords.broken;

import com.ladestitute.zeldaswordskills.registry.ItemInit;
import com.ladestitute.zeldaswordskills.util.ZeldaSwordSkillsKeyboardUtil;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.merchant.villager.VillagerEntity;
import net.minecraft.entity.merchant.villager.VillagerProfession;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.*;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.items.ItemHandlerHelper;

import java.util.List;

public class BrokenKokiriSwordItem extends Item
{
    public BrokenKokiriSwordItem(Properties builder)
    {
        super(builder.stacksTo(1));
    }

    @Override
    public void appendHoverText(ItemStack stack, World world, List<ITextComponent> list, ITooltipFlag flag)
    {
        super.appendHoverText(stack, world, list, flag);
        list.add(new StringTextComponent(TextFormatting.ITALIC + "A sword broken beyond repair"));
    }

    @Override
    public boolean onLeftClickEntity(ItemStack stack, PlayerEntity player, Entity entity) {
        if(entity instanceof VillagerEntity && !player.level.isClientSide)
        {
            //Todo: Gorons and/or custom trade window
            VillagerEntity villager = (VillagerEntity) entity;
            if(villager.getVillagerData().getProfession() == VillagerProfession.ARMORER)
            {
                if(villager.getVillagerData().getLevel() == 1)
                {
                    villager.getOffers().add(2, new MerchantOffer(new ItemStack(ItemInit.BROKEN_KOKIRI_SWORD.get()), new ItemStack(Items.EMERALD, 5), new ItemStack(ItemInit.KOKIRI_SWORD.get()), 999999999, 25, 0.05F));
                    player.displayClientMessage(new TranslationTextComponent("chat.zss.trade.sword.broken"), false);
                }
                if(villager.getVillagerData().getLevel() == 2)
                {
                    villager.getOffers().add(4, new MerchantOffer(new ItemStack(ItemInit.BROKEN_KOKIRI_SWORD.get()), new ItemStack(Items.EMERALD, 5), new ItemStack(ItemInit.KOKIRI_SWORD.get()), 999999999, 25, 0.05F));
                    player.displayClientMessage(new TranslationTextComponent("chat.zss.trade.sword.broken"), false);
                }
                if(villager.getVillagerData().getLevel() == 3)
                {
                    villager.getOffers().add(6, new MerchantOffer(new ItemStack(ItemInit.BROKEN_KOKIRI_SWORD.get()), new ItemStack(Items.EMERALD, 5), new ItemStack(ItemInit.KOKIRI_SWORD.get()), 999999999, 25, 0.05F));
                    player.displayClientMessage(new TranslationTextComponent("chat.zss.trade.sword.broken"), false);
                }
                if(villager.getVillagerData().getLevel() == 4)
                {
                    villager.getOffers().add(8, new MerchantOffer(new ItemStack(ItemInit.BROKEN_KOKIRI_SWORD.get()), new ItemStack(Items.EMERALD, 5), new ItemStack(ItemInit.KOKIRI_SWORD.get()), 999999999, 25, 0.05F));
                    player.displayClientMessage(new TranslationTextComponent("chat.zss.trade.sword.broken"), false);
                }
                if(villager.getVillagerData().getLevel() == 5)
                {
                    villager.getOffers().add(10, new MerchantOffer(new ItemStack(ItemInit.BROKEN_KOKIRI_SWORD.get()), new ItemStack(Items.EMERALD, 5), new ItemStack(ItemInit.KOKIRI_SWORD.get()), 999999999, 25, 0.05F));
                    player.displayClientMessage(new TranslationTextComponent("chat.zss.trade.sword.broken"), false);
                }
            }
            if(villager.getVillagerData().getProfession() == VillagerProfession.TOOLSMITH||
                    villager.getVillagerData().getProfession() == VillagerProfession.WEAPONSMITH)
            {
                if(villager.getVillagerData().getLevel() == 1)
                {
                    villager.getOffers().add(2, new MerchantOffer(new ItemStack(ItemInit.BROKEN_KOKIRI_SWORD.get()), new ItemStack(Items.EMERALD, 5), new ItemStack(ItemInit.KOKIRI_SWORD.get()), 999999999, 25, 0.05F));
                    player.displayClientMessage(new TranslationTextComponent("chat.zss.trade.sword.broken"), false);
                }
                if(villager.getVillagerData().getLevel() == 2)
                {
                    villager.getOffers().add(4, new MerchantOffer(new ItemStack(ItemInit.BROKEN_KOKIRI_SWORD.get()), new ItemStack(Items.EMERALD, 5), new ItemStack(ItemInit.KOKIRI_SWORD.get()), 999999999, 25, 0.05F));
                    player.displayClientMessage(new TranslationTextComponent("chat.zss.trade.sword.broken"), false);
                }
                if(villager.getVillagerData().getLevel() == 3)
                {
                    villager.getOffers().add(6, new MerchantOffer(new ItemStack(ItemInit.BROKEN_KOKIRI_SWORD.get()), new ItemStack(Items.EMERALD, 5), new ItemStack(ItemInit.KOKIRI_SWORD.get()), 999999999, 25, 0.05F));
                    player.displayClientMessage(new TranslationTextComponent("chat.zss.trade.sword.broken"), false);
                }
                if(villager.getVillagerData().getLevel() == 4)
                {
                    villager.getOffers().add(8, new MerchantOffer(new ItemStack(ItemInit.BROKEN_KOKIRI_SWORD.get()), new ItemStack(Items.EMERALD, 5), new ItemStack(ItemInit.KOKIRI_SWORD.get()), 999999999, 25, 0.05F));
                    player.displayClientMessage(new TranslationTextComponent("chat.zss.trade.sword.broken"), false);
                }
                if(villager.getVillagerData().getLevel() == 5)
                {
                    villager.getOffers().add(9, new MerchantOffer(new ItemStack(ItemInit.BROKEN_KOKIRI_SWORD.get()), new ItemStack(Items.EMERALD, 5), new ItemStack(ItemInit.KOKIRI_SWORD.get()), 999999999, 25, 0.05F));
                    player.displayClientMessage(new TranslationTextComponent("chat.zss.trade.sword.broken"), false);
                }
            }
        }
        return super.onLeftClickEntity(stack, player, entity);
    }

    @Override
    public ActionResultType interactLivingEntity(ItemStack p_111207_1_, PlayerEntity p_111207_2_, LivingEntity p_111207_3_, Hand p_111207_4_) {
        if(p_111207_3_ instanceof VillagerEntity && !p_111207_2_.level.isClientSide && p_111207_2_.isCrouching())
        {
            //Todo: Gorons and/or custom trade window
            VillagerEntity villager = (VillagerEntity) p_111207_3_;
           if(villager.getVillagerData().getProfession() == VillagerProfession.ARMORER)
           {
               if(villager.getVillagerData().getLevel() == 1)
               {
                   villager.getOffers().add(2, new MerchantOffer(new ItemStack(ItemInit.BROKEN_KOKIRI_SWORD.get()), new ItemStack(Items.EMERALD, 5), new ItemStack(ItemInit.KOKIRI_SWORD.get()), 999999999, 25, 0.05F));
                   p_111207_2_.displayClientMessage(new TranslationTextComponent("chat.zss.trade.sword.broken"), false);
               }
               if(villager.getVillagerData().getLevel() == 2)
               {
                   villager.getOffers().add(4, new MerchantOffer(new ItemStack(ItemInit.BROKEN_KOKIRI_SWORD.get()), new ItemStack(Items.EMERALD, 5), new ItemStack(ItemInit.KOKIRI_SWORD.get()), 999999999, 25, 0.05F));
                   p_111207_2_.displayClientMessage(new TranslationTextComponent("chat.zss.trade.sword.broken"), false);
               }
               if(villager.getVillagerData().getLevel() == 3)
               {
                   villager.getOffers().add(6, new MerchantOffer(new ItemStack(ItemInit.BROKEN_KOKIRI_SWORD.get()), new ItemStack(Items.EMERALD, 5), new ItemStack(ItemInit.KOKIRI_SWORD.get()), 999999999, 25, 0.05F));
                   p_111207_2_.displayClientMessage(new TranslationTextComponent("chat.zss.trade.sword.broken"), false);
               }
               if(villager.getVillagerData().getLevel() == 4)
               {
                   villager.getOffers().add(8, new MerchantOffer(new ItemStack(ItemInit.BROKEN_KOKIRI_SWORD.get()), new ItemStack(Items.EMERALD, 5), new ItemStack(ItemInit.KOKIRI_SWORD.get()), 999999999, 25, 0.05F));
                   p_111207_2_.displayClientMessage(new TranslationTextComponent("chat.zss.trade.sword.broken"), false);
               }
               if(villager.getVillagerData().getLevel() == 5)
               {
                   villager.getOffers().add(10, new MerchantOffer(new ItemStack(ItemInit.BROKEN_KOKIRI_SWORD.get()), new ItemStack(Items.EMERALD, 5), new ItemStack(ItemInit.KOKIRI_SWORD.get()), 999999999, 25, 0.05F));
                   p_111207_2_.displayClientMessage(new TranslationTextComponent("chat.zss.trade.sword.broken"), false);
               }
           }
            if(villager.getVillagerData().getProfession() == VillagerProfession.TOOLSMITH||
                    villager.getVillagerData().getProfession() == VillagerProfession.WEAPONSMITH)
            {
                if(villager.getVillagerData().getLevel() == 1)
                {
                    villager.getOffers().add(2, new MerchantOffer(new ItemStack(ItemInit.BROKEN_KOKIRI_SWORD.get()), new ItemStack(Items.EMERALD, 5), new ItemStack(ItemInit.KOKIRI_SWORD.get()), 999999999, 25, 0.05F));
                    p_111207_2_.displayClientMessage(new TranslationTextComponent("chat.zss.trade.sword.broken"), false);
                }
                if(villager.getVillagerData().getLevel() == 2)
                {
                    villager.getOffers().add(4, new MerchantOffer(new ItemStack(ItemInit.BROKEN_KOKIRI_SWORD.get()), new ItemStack(Items.EMERALD, 5), new ItemStack(ItemInit.KOKIRI_SWORD.get()), 999999999, 25, 0.05F));
                    p_111207_2_.displayClientMessage(new TranslationTextComponent("chat.zss.trade.sword.broken"), false);
                }
                if(villager.getVillagerData().getLevel() == 3)
                {
                    villager.getOffers().add(6, new MerchantOffer(new ItemStack(ItemInit.BROKEN_KOKIRI_SWORD.get()), new ItemStack(Items.EMERALD, 5), new ItemStack(ItemInit.KOKIRI_SWORD.get()), 999999999, 25, 0.05F));
                    p_111207_2_.displayClientMessage(new TranslationTextComponent("chat.zss.trade.sword.broken"), false);
                }
                if(villager.getVillagerData().getLevel() == 4)
                {
                    villager.getOffers().add(8, new MerchantOffer(new ItemStack(ItemInit.BROKEN_KOKIRI_SWORD.get()), new ItemStack(Items.EMERALD, 5), new ItemStack(ItemInit.KOKIRI_SWORD.get()), 999999999, 25, 0.05F));
                    p_111207_2_.displayClientMessage(new TranslationTextComponent("chat.zss.trade.sword.broken"), false);
                }
                if(villager.getVillagerData().getLevel() == 5)
                {
                    villager.getOffers().add(9, new MerchantOffer(new ItemStack(ItemInit.BROKEN_KOKIRI_SWORD.get()), new ItemStack(Items.EMERALD, 5), new ItemStack(ItemInit.KOKIRI_SWORD.get()), 999999999, 25, 0.05F));
                    p_111207_2_.displayClientMessage(new TranslationTextComponent("chat.zss.trade.sword.broken"), false);
                }
            }
        }
        return super.interactLivingEntity(p_111207_1_, p_111207_2_, p_111207_3_, p_111207_4_);
    }
}
