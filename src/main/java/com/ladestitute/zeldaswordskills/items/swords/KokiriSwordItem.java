package com.ladestitute.zeldaswordskills.items.swords;

import com.ladestitute.zeldaswordskills.registry.ItemInit;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.IItemTier;
import net.minecraft.item.ItemStack;
import net.minecraft.item.SwordItem;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;
import net.minecraftforge.items.ItemHandlerHelper;

import java.util.List;

public class KokiriSwordItem extends SwordItem
{
    public KokiriSwordItem(IItemTier tier, int attackDamageIn, float attackSpeedIn, Properties builder)
    {
        super(tier, attackDamageIn, attackSpeedIn, builder.stacksTo(1));
    }

    @Override
    public void appendHoverText(ItemStack stack, World world, List<ITextComponent> list, ITooltipFlag flag)
    {
        super.appendHoverText(stack, world, list, flag);
        list.add(new StringTextComponent(TextFormatting.ITALIC + "A sword from Kokiri Forest"));
    }


    @Override
    public void inventoryTick(ItemStack p_77663_1_, World p_77663_2_, Entity p_77663_3_, int p_77663_4_, boolean p_77663_5_) {
        if(p_77663_1_.getDamageValue() >= 256)
        {
            PlayerEntity player = (PlayerEntity) p_77663_3_;
            player.playNotifySound(SoundEvents.ITEM_BREAK, SoundCategory.PLAYERS, 1f, 1f);
           p_77663_1_.shrink(1);
                ItemHandlerHelper.giveItemToPlayer((PlayerEntity) p_77663_3_, ItemInit.BROKEN_KOKIRI_SWORD.get().getDefaultInstance(), ((PlayerEntity) p_77663_3_).inventory.selected);

        }
        super.inventoryTick(p_77663_1_, p_77663_2_, p_77663_3_, p_77663_4_, p_77663_5_);
    }
}
