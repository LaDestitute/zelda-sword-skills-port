package com.ladestitute.zeldaswordskills;

import com.ladestitute.zeldaswordskills.event.ZeldaSwordSkillsMainEventHandler;
import com.ladestitute.zeldaswordskills.registry.ItemInit;
import net.minecraft.entity.merchant.villager.VillagerProfession;
import net.minecraft.entity.merchant.villager.VillagerTrades;
import net.minecraft.item.*;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.event.village.VillagerTradesEvent;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.IForgeRegistry;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Mod(ZeldaSwordSkillsMain.MOD_ID)
@EventBusSubscriber(modid = ZeldaSwordSkillsMain.MOD_ID, bus = Bus.MOD)
public class ZeldaSwordSkillsMain
{
    public static ZeldaSwordSkillsMain instance;
    public static final String MOD_NAME = "Zelda Sword Skills";
    public static final String MOD_ID = "zeldaswordskills";
    public static final Logger LOGGER = LogManager.getLogger();

    public ZeldaSwordSkillsMain()
    {
        instance = this;
      //  ModLoadingContext.get().registerConfig(ModConfig.Type.COMMON, TemplateConfig.SPEC, "templatemodconfig.toml");
        final IEventBus modEventBus = FMLJavaModLoadingContext.get().getModEventBus();
        MinecraftForge.EVENT_BUS.addListener(this::villagerTrades);

        //Uncomment if you are adding structures (TelepathicGrunt has an example-github on how)
        //Structures.DEFERRED_REGISTRY_STRUCTURE.register(modEventBus);
        modEventBus.addListener(this::setup);

        DistExecutor.unsafeRunWhenOn(Dist.CLIENT, () -> () ->
        {

        });
        //Example mod event bus
       MinecraftForge.EVENT_BUS.register(new ZeldaSwordSkillsMainEventHandler());
     //   MinecraftForge.EVENT_BUS.register(new TemplateWorldGen());
        /* Register all of our deferred registries from our list/init classes, which get added to the IEventBus */
        //Some example registries to setup a basic mod
     //   TemplateDropModifier.GLM.register(modEventBus);
    //    SoundInit.SOUNDS.register(modEventBus);
        ItemInit.ITEMS.register(modEventBus);
     //   SpecialBlockInit.SPECIAL_BLOCKS.register(modEventBus);
     //   BlockInit.BLOCKS.register(modEventBus);
    }

    //Helper method to automatically register blocks and their item-forms
    @SubscribeEvent
    public static void createBlockItems(final RegistryEvent.Register<Item> event) {
        final IForgeRegistry<Item> registry = event.getRegistry();

//        BlockInit.BLOCKS.getEntries().stream().map(RegistryObject::get).forEach(block -> {
//            final Item.Properties properties = new Item.Properties().tab(TemplateMain.BLOCKS_TAB);
//            final BlockItem blockItem = new BlockItem(block, properties);
//            blockItem.setRegistryName(block.getRegistryName());
//            registry.register(blockItem);
//        });
    }


    /* The FMLCommonSetupEvent (FML - Forge Mod Loader) */
    private void setup(final FMLCommonSetupEvent event)
    {
        event.enqueueWork(() -> {
            //Uncomment if you are adding structures (TelepathicGrunt has an example-github on how)
            // BTDStructures.setupStructures();
            // BTDConfiguredStructures.registerConfiguredStructures();
          //  ConfiguredFeatureInit.registerConfiguredFeatures();
        });
    }

    // Custom CreativeModeTab tab
    public static final ItemGroup COMBAT_TAB = new ItemGroup("zeldaswordskills_combat") {
        @Override
        public ItemStack makeIcon() {
            return new ItemStack(ItemInit.MASTER_SWORD.get());
        }
    };

    private void villagerTrades(VillagerTradesEvent event)
    {
        if(event.getType() == VillagerProfession.TOOLSMITH||
                event.getType() == VillagerProfession.WEAPONSMITH||
                event.getType() == VillagerProfession.ARMORER)
        {
            event.getTrades().get(1).add(ZeldaSwordSkillsMain.buildTrade(new ItemStack(Items.EMERALD, 16),
                    new ItemStack(ItemInit.KOKIRI_SWORD.get(), 1), 999999999, 25, 0.05F));
        }

    }

    public static VillagerTrades.ITrade buildTrade(ItemStack wanted1, ItemStack given, int tradesUntilDisabled, int xpToVillagr, float priceMultiplier)
    {
        return ZeldaSwordSkillsMain.buildTrade(wanted1, ItemStack.EMPTY, given, tradesUntilDisabled, xpToVillagr, priceMultiplier);
    }

    public static VillagerTrades.ITrade buildTrade(ItemStack wanted1, ItemStack wanted2, ItemStack given, int tradesUntilDisabled, int xpToVillagr, float priceMultiplier)
    {
        return (entity, random) -> new MerchantOffer(wanted1, wanted2, given, tradesUntilDisabled, xpToVillagr, priceMultiplier);
    }



}

